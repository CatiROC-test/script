# Update path before launching shell under emacs

# (getenv "LD_LIBRARY_PATH")
# (setenv "LD_LIBRARY_PATH" "/home/csantos/Projects/apc/juno/CatiROC-test-script/software/")

a =!= 4

using Winston
x = linspace(0, 3pi, 100);
c = cos(x);
s = sin(x);
p = FramedPlot();
setattr(p, "title", "title!")
setattr(p, "xlabel", L"\Sigma x^2_i")
setattr(p, "ylabel", L"\Theta_i")
add(p, FillBetween(x, c, x, s) )
add(p, Curve(x, c, "color", "red") )
add(p, Curve(x, s, "color", "blue") )


# * Read

push!(LOAD_PATH, "/home/csantos/Projects/apc/juno/CatiROC-test-script/src");
using TestCardModule

# using ConfigurationModule

# * Methods

myTestCard = TestCard();

tc_GetIsOpen(myTestCard)

tc_Purge(myTestCard)

# tc_Initialize(myTestCard)

tc_GetRxStatus(myTestCard)

tc_UpdateConfig(myTestCard)

tc_Finalize(myTestCard)

# * Send a command

# command = collect(UInt8, linspace(1,10,10));
# command[1] = 5;
# command[end] = 255;
# (status, BytesWritten) = tc_SendCommand(myTestCard, command);

# * Read

# BufferOut = zeros(UInt8, 10);
# (status, BytesRead) = tc_Read(myTestCard, BufferOut)

# * Tests

# ** Test bandwidth

# for i = 1:100
#     ok = tc_TestRandomData(myTestCard, 70);
#     println("Test $ok")
#     sleep(1)
# end
