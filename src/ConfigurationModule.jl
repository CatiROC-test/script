# Update path before launching shell under emacs

# (getenv "LD_LIBRARY_PATH")
# (setenv "LD_LIBRARY_PATH" "/home/csantos/Projects/apc/juno/CatiROC-test-script/software/ftdi_linux")

# README:

# All functions return
# - "status", or a tuple (status, out) when appropriate
# - -1 when something goes wrong (handle not available, etc)

# * Module

module ConfigurationModule

# ** Export

export TestCard, tc_Finalize, tc_GetIsOpen, tc_Purge, tc_Initialize,
    tc_SendCommand, tc_Read, tc_GetRxStatus, tc_TestRandomData

end # module
