# Update path before launching shell under emacs

# (getenv "LD_LIBRARY_PATH")
# (setenv "LD_LIBRARY_PATH" "/home/csantos/Projects/apc/juno/CatiROC-test-script/software/ftdi_linux")

# README:

# All functions return
# - "status", or a tuple (status, out) when appropriate
# - -1 when something goes wrong (handle not available, etc)

# * Module

module TestCardModule

# ** Configuration type

# *** Helper types

# **** EnPpType

type EnPpType
    en :: String
    pp :: String
    EnPpType() = new("0", "0");
end

# **** FormatValueType

type FormatValueType
    format :: String
    value :: String
    FormatValueType() = new("", "");
end

# **** SwCfType

type SwCfType
    format :: String
    value
    off_PA_HG :: String
    off_PA_LG :: String
    SwCfType() = new("null", -1, "0", "0");
end

# **** HgLgEnPpType

type HgLgEnPpType
    HG :: EnPpType
    LG :: EnPpType
    HgLgEnPpType() = (
        HG = EnPpType();
        LG = EnPpType();
        new(HG, LG);
    )
end

# *** Physical types

# **** AddCompensatedSwitchesType

type AddCompensatedSwitchesType
    # Add compensated switches on time ADC ramp, 1 bit
    TimeADCRamp :: String
    # Add compensated switches on charge ADC ramp, 1 bit
    ChargeADCRamp :: String
    AddCompensatedSwitchesType() = new("0", "0");
end

# **** FastShapperType

type FastShapperType
    PowerOn :: EnPpType
    FastShapperType() = (
        PowerOn = EnPpType();
        new(PowerOn);
    )
end

# **** SwitchOffType

type SwitchOffType
    LVDSReceiver160MHz :: String
    LVDSReceiver40MHz :: String
    OTA :: String
    SwitchOffType() = new("0", "0", "0");
end

# **** PowerOnType

type PowerOnType
    Preamp :: HgLgEnPpType
    GainDiscri :: EnPpType
    TimeDiscri :: EnPpType
    TDC :: EnPpType
    ChargeADC :: EnPpType
    TimeADC :: EnPpType
    DACDelay :: EnPpType
    DAC :: EnPpType
    RampChargeADC :: EnPpType
    RampTimeADC :: EnPpType
    OTA :: EnPpType
    SlowLVDSReceiver :: EnPpType
    LVDSTransceiver :: EnPpType
    PowerOnType() = (
        HgLgEnPp = HgLgEnPpType();
        GainDiscri = EnPpType();
        TimeDiscri = EnPpType();
        TDC = EnPpType();
        ChargeADC = EnPpType();
        TimeADC = EnPpType();
        DACDelay = EnPpType();
        DAC = EnPpType();
        RampChargeADC = EnPpType();
        RampTimeADC = EnPpType();
        OTA = EnPpType();
        SlowLVDSReceiver = EnPpType();
        LVDSTransceiver = EnPpType();
        new(HgLgEnPp, GainDiscri, TimeDiscri, TDC, ChargeADC, TimeADC, DACDelay,
            DAC, RampChargeADC, RampTimeADC, OTA, SlowLVDSReceiver, LVDSTransceiver);
    )
end

# **** DiscriType

type DiscriType
    TimeDAC :: FormatValueType
    ChargeDAC :: FormatValueType
    DiscriType() = (
        TimeDAC = FormatValueType();
        ChargeDAC = FormatValueType();
        new(TimeDAC, ChargeDAC);
    )
end

# **** HgLgFormatValueType

type HgLgFormatValueType
    HG :: FormatValueType
    LG :: FormatValueType
    HgLgFormatValueType() = (
        HG = FormatValueType();
        LG = FormatValueType();
        new(HG, LG);
    )
end

# **** SlowShapperType

type SlowShapperType
    Gain :: HgLgFormatValueType
    PowerOn :: HgLgEnPpType
    Feedback :: HgLgFormatValueType
    TimeConstant :: HgLgFormatValueType
    PowerOnSCA :: EnPpType
    Sent :: Array{FormatValueType, 1}
    SlowShapperType() = (
        Gain = HgLgFormatValueType();
        Feedback = HgLgFormatValueType();
        PowerOn = HgLgEnPpType();
        TimeConstant = HgLgFormatValueType();
        PowerOnSCA = EnPpType();
        # Sent
        Sent = FormatValueType[];
        for i = 1:16
        push!(Sent, FormatValueType())
        end;
        new(Gain, PowerOn, Feedback, TimeConstant, PowerOnSCA, Sent);
    )
end

# **** ProbeType

type ProbeType
    data :: FormatValueType
    dig2 :: FormatValueType
    fsh  :: FormatValueType
    dig1 :: FormatValueType
    ssh  :: FormatValueType
    pa   :: FormatValueType
    dig0 :: String
    dig4 :: String
    ProbeType() = (
        data = FormatValueType();
        dig2 = FormatValueType();
        fsh  = FormatValueType();
        dig1 = FormatValueType();
        ssh  = FormatValueType();
        pa   = FormatValueType();
        dig0 = "";
        dig4 = "";
        new(data, dig2, fsh, dig1, ssh, pa, dig0, dig4);
    )
end

# *** TestCardConfigurationType

type TestCardConfigurationType
    verbose                :: String
    EnableInputTestChannel :: FormatValueType
    sw_cf                  :: Array{SwCfType, 1}
    PowerOn                :: PowerOnType
    SlowShapper            :: SlowShapperType
    Discri                 :: DiscriType
    Mask                   :: FormatValueType
    Delay                  :: FormatValueType
    SelData                :: FormatValueType
    ChargeADCRampSlope     :: String
    TimeADCRampSlope       :: String
    SelClkDiv4             :: String
    Sel80M                 :: String
    DisableOvfCpt          :: String
    SelExtRazChannel       :: String
    SelExtRead             :: String
    EnableTacReadout       :: String
    EnableNOR16            :: String
    EnableTransmit         :: String
    EnableData_oc          :: String
    DisableTriggerBuffer   :: String
    SwitchOff              :: SwitchOffType
    AddCompensatedSwitches :: AddCompensatedSwitchesType
    FastShapper            :: FastShapperType
    SwitchTxBias           :: FormatValueType
    Probe                  :: ProbeType
    TestCardConfigurationType() = (
        verbose                = "";
        EnableInputTestChannel = FormatValueType();
        sw_cf                  = SwCfType[];
        for i                  = 1:16
        push!(sw_cf, SwCfType())
        end;
        PowerOn                = PowerOnType();
        SlowShapper            = SlowShapperType();
        Discri                 = DiscriType();
        Mask                   = FormatValueType();
        Delay                  = FormatValueType();
        SelData                = FormatValueType();
        ChargeADCRampSlope     = "";
        TimeADCRampSlope       = "";
        SelClkDiv4             = "";
        Sel80M                 = "";
        DisableOvfCpt          = "";
        SelExtRazChannel       = "";
        SelExtRead             = "";
        EnableTacReadout       = "";
        EnableNOR16            = "";
        EnableTransmit         = "";
        EnableData_oc          = "";
        DisableTriggerBuffer   = "";
        SwitchOff              = SwitchOffType();
        AddCompensatedSwitches = AddCompensatedSwitchesType();
        FastShapper            = FastShapperType();
        SwitchTxBias           = FormatValueType();
        Probe                  = ProbeType();
        new("1", EnableInputTestChannel, sw_cf, PowerOn, SlowShapper, Discri, Mask, Delay, SelData,
            ChargeADCRampSlope, TimeADCRampSlope, SelClkDiv4, Sel80M,
            DisableOvfCpt, SelExtRazChannel, SelExtRead, EnableTacReadout,
            EnableNOR16, EnableTransmit, EnableData_oc, DisableTriggerBuffer,
            SwitchOff, AddCompensatedSwitches, FastShapper, SwitchTxBias, Probe);
    )
end

# ** Test Card Type

# *** Type

type TestCard
    NumberDevices       :: UInt64
    IsOpen              :: UInt64
    DeviceNumber        :: UInt64
    status              :: Int64
    configuration       :: TestCardConfigurationType
    configuration_array :: Array{String,1};
    TestCard(NumberDevices, IsOpen, DeviceNumber, status) = (
        # Initialize
        (status, NumberDevices, DeviceNumber, IsOpen) = tc_Init();
        configuration = TestCardConfigurationType();
        configuration_array = repmat(["a"], 328+200);
        new(NumberDevices, IsOpen, DeviceNumber, status, configuration, configuration_array);
    )
end

TestCard() = TestCard(0, 0, 0, 0);

# *** Init

function tc_Init()
    # Defaults
    NumberDevices = 0;
    IsOpen = 0;
    DeviceNumber = -1;
    status = -1;
    # Check device present
    NumberDevices = Ref{Culong}(5)
    status = ccall((:GetNumberDevs, "../software/libftdi_linux"),
                   Culong,
                   (Ref{Culong},),
                   NumberDevices)
    if status == 0
        if NumberDevices.x > 0
            DeviceNumber = convert(Int, NumberDevices.x) - 1;
            # Init USB
            status = ccall((:InitializeUSB, "../software/libftdi_linux"),
                           Culong,
                           (Culong,),
                           DeviceNumber);
            if status == 0
                # Config USB
                status = ccall((:ConfigureUSB, "../software/libftdi_linux"),
                               Culong,
                               (Culong,),
                               DeviceNumber);
                if status == 0
                    IsOpen = Ref{Culong}(5);
                    status = ccall((:GetIsOpen, "../software/libftdi_linux"),
                                   Culong,
                                   (Culong, Ref{Culong}),
                                   0,
                                   IsOpen);
                else
                    println("Error ConfigureUSB :: status $status")
                end;
            else
                println("Error InitializeUSB :: status $status")
            end;
        else
            println("Error No devices found")
            # return (convert(Int, status), 0, DeviceNumber, IsOpen)
            return (0, 0, 0, 0)
        end;
    else
        println("Error GetNumberDevs :: status $status")
    end
    #
    if isa(IsOpen, Base.RefValue{UInt64})
        IsOpen = IsOpen.x;
    end;
    if isa(NumberDevices, Base.RefValue{UInt64})
        NumberDevices = NumberDevices.x;
    end;
    return (convert(Int, status), convert(Int, NumberDevices), DeviceNumber, IsOpen)
end

# *** Initialize

function tc_Initialize(card::TestCard)
    if card.IsOpen == 0
        # Check device present
        NumberDevices = Ref{Culong}(5);
        status = ccall((:GetNumberDevs, "../software/libftdi_linux"),
                       Culong,
                       (Ref{Culong},),
                       NumberDevices);
        if status == 0
            card.NumberDevices = convert(Int, NumberDevices.x);
            if NumberDevices.x > 0
                card.DeviceNumber = convert(Int, NumberDevices.x) - 1;
                # Init USB
                status = ccall((:InitializeUSB, "../software/libftdi_linux"),
                               Culong,
                               (Culong,),
                               card.DeviceNumber);
                if status == 0
                    # Config USB
                    status = ccall((:ConfigureUSB, "../software/libftdi_linux"),
                                   Culong,
                                   (Culong,),
                                   0);
                    if status == 0
                        IsOpen = Ref{Culong}(5);
                        status = ccall((:GetIsOpen, "../software/libftdi_linux"),
                                       Culong,
                                       (Culong, Ref{Culong}),
                                       0,
                                       IsOpen);
                        card.IsOpen = IsOpen.x;
                    else
                        println("Error ConfigureUSB :: status $status")
                    end;
                else
                    println("Error InitializeUSB :: status $status")
                end;
            else
                card.DeviceNumber = convert(Int, -1);
                println("Error No devices found")
                return -1
            end;
        else
            card.NumberDevices = convert(Int, 0);
            println("Error GetNumberDevs :: status $status")
        end
    else
        println("Device already initialized.")
        return -1;
    end
    return convert(Int, status)
end

# *** Finalize

function tc_Finalize(card::TestCard)
    if card.IsOpen == 1
        status = ccall((:FinalizeUSB, "../software/libftdi_linux"),
                       Culong,
                       (Culong,),
                       card.DeviceNumber)
        if status == 0
            (status, IsOpen) = tc_GetIsOpen(card);
        else
            println("Error FinalizeUSB :: status $status")
        end
        return convert(Int, status)
    else
        println("Device not open.")
        return -1;
    end
end

# *** Get is open

function tc_GetIsOpen(card::TestCard)
    IsOpen = Ref{Culong}(5);
    status = ccall((:GetIsOpen, "../software/libftdi_linux"),
                   Culong,
                   (Culong, Ref{Culong}),
                   0,
                   IsOpen);
    if status == 0
        card.IsOpen = IsOpen.x;
    else
        card.IsOpen = 0;
        println("Error GetISOpen :: status $status")
    end
    return (convert(Int, status), card.IsOpen);
end

# *** Purge

"""
Delete any trailing data around in usb low level buffers.
"""

function tc_Purge(card::TestCard)
    if card.IsOpen == 1
        status = ccall((:PurgeRxTxUSB, "../software/libftdi_linux"),
                       Culong,
                       (Culong,),
                       card.DeviceNumber)
        return convert(Int, status)
    else
        println("Device not open.")
        return -1;
    end
end

# *** Send command

"""
Send a command to the card.

# Example

  command = collect(UInt8, linspace(1,10,10));
  command[1] = 5;
  command[end] = 255;
  (status, BytesWritten) = tc_SendCommand(myTestCard, command);

"""
function tc_SendCommand(card::TestCard, command)
    if card.IsOpen == 1
        status = tc_Purge(card);
        BytesWritten = Ref{Culong}(5);
        status = ccall((:WriteUSB, "../software/libftdi_linux"),
                       Culong,
                       (Culong, Ptr{Void}, Culong, Ref{Culong}),
                       card.DeviceNumber,
                       command,
                       length(command),
                       BytesWritten)
        return (convert(Int, status), convert(Int, BytesWritten.x))
    else
        println("Device not open.")
        return -1;
    end
end

# *** Read data

"""
Read data back from the card.

# Example

  DataBuffer = zeros(UInt8, 10);
  (status, BytesRead) = tc_Read(myTestCard, DataBuffer)

"""

function tc_Read(card::TestCard, BufferOut)
    if card.IsOpen == 1
        BytesRead = Ref{Culong}(5);
        status = ccall((:ReadUSB, "../software/libftdi_linux"),
                       Culong,
                       (Culong, Ptr{Void}, Culong, Ref{Culong}),
                       card.DeviceNumber,
                       BufferOut,
                       length(BufferOut),
                       BytesRead)
        return (convert(Int, status), convert(Int, BytesRead.x))
    else
        println("Device not open.")
        return -1;
    end
end

# *** Get status

"""
  Query the number of available bytes to read.
"""

function tc_GetRxStatus(card::TestCard)
    AmountInRxQueue = Ref{Culong}(5);
    AmountInTxQueue = Ref{Culong}(5);
    EventStatus = Ref{Culong}(5);
    status = ccall((:GetRxTxStatusUSB, "../software/libftdi_linux"), Culong,
                   (Culong, Ref{Culong}, Ref{Culong}, Ref{Culong}),
                   card.DeviceNumber,
                   AmountInRxQueue,
                   AmountInTxQueue,
                   EventStatus)
    return (convert(Int, status), convert(Int, AmountInRxQueue.x))
end


# *** Tests

# **** Bandwidth

function tc_TestRandomData(card::TestCard, L=200)
    if tc_Purge(card)==0
        # command = collect(UInt8, linspace(1,L,L));
        command = rand(UInt8, L);
        command[1] = 5;
        command[end] = 255;
        (status, BytesWritten) = tc_SendCommand(card, command);
        if BytesWritten==L
            BufferOut = zeros(UInt8, L);
            (status, BytesRead) = tc_Read(card, BufferOut)
            if status == 0 & (command == BufferOut)
                return true
            else
                return BufferOut
            end
        end
    end
end

# *** Configure

# **** Update configuration

# using this eval(parse("myTestCard.configuration.sw_cf[1].format = \"bin\""))

function tc_UpdateConfig()
    # include("catiroc_parameters.param")
    # fid = open("configuration/catiroc_parameters.param")
    # while !eof(fid)
    #     linea = readline(fid)
    #     println("$linea")
    #     # println(linea)
    #     if isempty(linea)
    #         continue
    #     end
    #     # compare '#'
    #     if isequal('\u0023', linea[1])
    #         continue
    #     end
    #     if ismatch(r" = ", linea)
    #         m = match(r" = ", linea);
    #         PosEqual = m.offset;
    #     end
    #     # PosEqual      = strfind(linea,'=');
    #     if ismatch(r";", linea)
    #         m = match(r";", linea);
    #         PosSemiColom = m.offset;
    #     end
    #     parameter = linea[1:PosEqual-1];
    #     println("$parameter")
    #     value     = linea[PosEqual+3:PosSemiColom-1];
    #     println("$value")
    #     # if (value == "1" || value == "0")
    #     #     value = value == "1" ? true : false;
    #     #     eval(parse("configuration.$parameter = $value "))
    #     # else
    #     # mystruct = "card.configuration";
    #     # mystruct = card.configuration;
    #     if ismatch(r"\.", parameter)
    #         m = match(r"\.", parameter);
    #         PosDot = m.offset;
    #         # mystruct = mystruct * "." * parameter[1:PosDot-1];
    #         parameter1 = parameter[1:PosDot-1];
    #         parameter2 = parameter[PosDot+1:end];
    #         setfield!(getfield(card.configuration, Symbol(parameter1)),
    #                   Symbol(parameter2), value)
    #     else
    #         setfield!(card.configuration, Symbol(parameter), value)
    #     end
    #     # setfield!(eval(parse(mystruct)), Symbol(parameter), value)
    #     # setfield!(mystruct, Symbol(parameter), value)
    #     # end;
    # end
    # close(fid)
end

# **** Send configuration

function tc_SendConfig(card::TestCard)
    tc_UpdateConfig(card)
    # if eval(obj.configuration.SelClkDiv4)
    #     %% internal clock
    #     obj.SendCommand([10,1,255]);
    #     fprintf('\n\tDisabled sending 40 MHz clock.\n')
    # else
    #     %% external clock
    #     obj.SendCommand([10,0,255]);
    #     fprintf('\n\tEnabled sending 40 MHz clock.\n')
    # end
end

# **** Update configuration array

function tc_updateconfigarray(card::TestCard)

    # obj = obj.RegisterToConfigArray_value(config.EnableInputTestChannel, 0, 16, 1, 0);

    # obj = obj.RegisterToConfigArray_value_ext(config.sw_cf, 16, 8, 16, 1);

    # obj = obj.RegisterToConfigArray_en_pp(config.PowerOn.Preamp.HG, 176);

    # obj = obj.RegisterToConfigArray_en_pp(config.PowerOn.Preamp.LG, 178);

    # obj = obj.RegisterToConfigArray_en_pp(config.SlowShapper.PowerOn.HG, 180);

    # obj = obj.RegisterToConfigArray_value(config.SlowShapper.Gain.HG, 182, 2, 1, 1);

    # obj = obj.RegisterToConfigArray_value(config.SlowShapper.TimeConstant.HG, 184, 3, 1, 1);

    # obj = obj.RegisterToConfigArray_en_pp(config.SlowShapper.PowerOn.LG, 187);

    # obj = obj.RegisterToConfigArray_value(config.SlowShapper.Gain.LG, 189, 2, 1, 1);

    # obj = obj.RegisterToConfigArray_value(config.SlowShapper.TimeConstant.LG, 191, 3, 1, 1);

    # obj = obj.RegisterToConfigArray_value(config.SlowShapper.Feedback.HG, 194, 3, 1, 1);

    # obj = obj.RegisterToConfigArray_value(config.SlowShapper.Feedback.LG, 197, 3, 1, 1);

    # obj = obj.RegisterToConfigArray_en_pp(config.SlowShapper.PowerOnSCA, 200);

    # card.configuration_array(202 + 1) = config.ModeSelection.Gain;

    # obj = obj.RegisterToConfigArray_en_pp(config.PowerOn.GainDiscri, 203);

    # obj = obj.RegisterToConfigArray_en_pp(config.FastShaper.PowerOn, 205);

    # card.configuration_array(207 + 1) = config.ModeSelection.Hold;

    # obj = obj.RegisterToConfigArray_en_pp(config.PowerOn.TimeDiscri, 208);

    # obj = obj.RegisterToConfigArray_en_pp(config.PowerOn.TDC, 210);

    # obj = obj.RegisterToConfigArray_en_pp(config.PowerOn.ChargeADC, 212);

    # obj = obj.RegisterToConfigArray_en_pp(config.PowerOn.TimeADC, 214);

    # obj = obj.RegisterToConfigArray_value(config.SelData, 216, 2, 1, 0);

    # obj = obj.RegisterToConfigArray_en_pp(config.PowerOn.DACDelay, 218);

    # obj = obj.RegisterToConfigArray_value(config.Delay, 220, 8, 1, 1);

    # obj = obj.RegisterToConfigArray_value(config.Mask, 228, 16, 1, 0);

    # obj = obj.RegisterToConfigArray_value(config.SlowShapper.Sent, 244, 2, 16, 0);

    # obj = obj.RegisterToConfigArray_en_pp(config.PowerOn.DAC, 276);

    # obj = obj.RegisterToConfigArray_value(config.Discri.TimeDAC, 278, 10, 1, 1);

    # obj = obj.RegisterToConfigArray_value(config.Discri.ChargeDAC, 288, 10, 1, 1);

    # obj = obj.RegisterToConfigArray_en_pp(config.PowerOn.RampChargeADC, 298);

    # card.configuration_array(300 + 1) = config.AddCompensatedSwitches.ChargeADCRamp;

    # card.configuration_array(301 + 1) = config.ChargeADCRampSlope;

    # obj = obj.RegisterToConfigArray_en_pp(config.PowerOn.RampTimeADC, 302);

    # card.configuration_array(304 + 1) = config.AddCompensatedSwitches.TimeADCRamp;

    # card.configuration_array(305 + 1) = config.TimeADCRampSlope;

    # obj = obj.RegisterToConfigArray_en_pp(config.PowerOn.OTA, 306);

    # card.configuration_array(308 + 1) = config.SwitchOff.OTA;

    # obj = obj.RegisterToConfigArray_en_pp(config.PowerOn.SlowLVDSReceiver, 309);

    # card.configuration_array(311 + 1) = config.SwitchOff.LVDSReceiver40MHz;

    # card.configuration_array(312 + 1) = config.SwitchOff.LVDSReceiver160MHz;

    # card.configuration_array(313 + 1) = config.SelClkDiv4;

    # card.configuration_array(314 + 1) = config.Sel80M;

    # card.configuration_array(315 + 1) = config.DisableOvfCpt;

    # card.configuration_array(316 + 1) = config.SelExtRazChannel;

    # card.configuration_array(317) = 1;

    # card.configuration_array(318 + 1) = config.SelExtRead;

    # card.configuration_array(319 + 1) = config.EnableTacReadout;

    # card.configuration_array(320 + 1) = config.EnableNOR16;

    # card.configuration_array(321 + 1) = config.EnableTransmit;

    # card.configuration_array(322 + 1) = config.EnableData_oc;

    # card.configuration_array(323 + 1) = config.DisableTriggerBuffer;

    # obj = obj.RegisterToConfigArray_en_pp(config.PowerOn.LVDSTransceiver, 324);

    # obj = obj.RegisterToConfigArray_value(config.SwitchTxBias, 326, 2, 1, 0);

    # # Probe Registers

    # obj = obj.RegisterToConfigArray_value(config.probe.data, 328+0,   32, 1, 0);

    # obj = obj.RegisterToConfigArray_value(config.probe.dig2, 328+32,  32, 1, 0);

    # obj = obj.RegisterToConfigArray_value(config.probe.fsh,  328+64,  16, 1, 0);

    # obj = obj.RegisterToConfigArray_value(config.probe.dig1, 328+80,  48, 1, 0);

    # obj = obj.RegisterToConfigArray_value(config.probe.ssh,  328+128, 32, 1, 0);

    # obj = obj.RegisterToConfigArray_value(config.probe.pa,   328+160, 32, 1, 0);

    # card.configuration_array(328 + 192 + 1) = config.probe.dig0;

    # card.configuration_array(328 + 193 + 1) = config.probe.dig4;
end

# **** Configuration helpers

function tc_RegisterToConfigArray_value(card::TestCard, register, LowestBit, Bits, Loop, revert)
    # for i = 1:Loop
    #     if register(i).format == "dec"
    #         if revert
    #             card.configuration_array(LowestBit+1:LowestBit+Bits) = bin(parse(register(i).value), Bits);
    #         else
    #             card.configuration_array(LowestBit+1:LowestBit+Bits) = reverse(bin(parse(register(i).value), Bits));
    #         end
    #     elseif register(i).format == "bin"
    #         if Bits ~= length(register(i).value)
    #             error("register %s, bin format", register(i))
    #         end
    #         if revert
    #             card.configuration_array(LowestBit+1:LowestBit+Bits) = register(i).value;
    #         else
    #             card.configuration_array(LowestBit+1:LowestBit+Bits) = reverse(register(i).value);
    #         end
    #     elseif register(i).format == "hex"
    #         if revert
    #             card.configuration_array(LowestBit+1:LowestBit+Bits) = bin(hex2dec(register(i).value), Bits);
    #         else
    #             card.configuration_array(LowestBit+1:LowestBit+Bits) = reverse(bin(hex2dec(register(i).value), Bits));
    #         end
    #     end
    #     LowestBit = LowestBit + Bits;
    # end
end

function tc_RegisterToConfigArray_value_ext(card::TestCard, register, LowestBit, Bits, Loop, revert)
    # for i = 1:Loop
    #     if register(i).format == "dec"
    #         if revert
    #             card.configuration_array(LowestBit+1:LowestBit+Bits) = bin(str2num(register(i).value), Bits);
    #         else
    #             card.configuration_array(LowestBit+1:LowestBit+Bits) = fliplr(bin(str2num(register(i).value), Bits));
    #         end
    #     elseif register(i).format == "bin"
    #         if Bits ~= length(register(i).value)
    #             error("register %s, bin format", register(i))
    #         end
    #         if revert
    #             card.configuration_array(LowestBit+1:LowestBit+Bits) = register(i).value;
    #         else
    #             card.configuration_array(LowestBit+1:LowestBit+Bits) = fliplr(register(i).value);
    #         end
    #     elseif register(i).format == "hex"
    #         if revert
    #             card.configuration_array(LowestBit+1:LowestBit+Bits) = bin(hex2dec(register(i).value), Bits);
    #         else
    #             card.configuration_array(LowestBit+1:LowestBit+Bits) = fliplr(bin(hex2dec(register(i).value), Bits));
    #         end
    #     end
    #     card.configuration_array(LowestBit + Bits + 1) = register(i).off_PA_HG;
    #     card.configuration_array(LowestBit + Bits + 2) = register(i).off_PA_LG;
    #     LowestBit = LowestBit + Bits + 2;
    # end
end

function tc_RegisterToConfigArray_en_pp(card::TestCard, register, LowestBit)
    # card.configuration_array(LowestBit + 1) = register.en;
    # card.configuration_array(LowestBit + 2) = register.pp;
end

# ** Export

export TestCard, tc_Finalize, tc_GetIsOpen, tc_Purge, tc_Initialize, tc_UpdateConfig,
    tc_SendCommand, tc_Read, tc_GetRxStatus, tc_TestRandomData, tc_SendConfig

end # module


# * Reference API code

# # Status

# NumberDevices = Ref{Culong}(5)
# status = ccall((:GetNumberDevs, "../software/libftdi_linux"), Culong, (Ref{Culong},), NumberDevices)

# AmountInRxQueue = Ref{Culong}(5);
# AmountInTxQueue = Ref{Culong}(5);
# EventStatus = Ref{Culong}(5);

# status = ccall((:GetRxTxStatusUSB, "../software/libftdi_linux"), Culong,
#                (Culong, Ref{Culong}, Ref{Culong}, Ref{Culong}),
#                0, AmountInRxQueue, AmountInTxQueue, EventStatus)

# # Open / close peripheral

# status = ccall((:FinalizeUSB, "../software/libftdi_linux"), Culong, (Culong,), 0)
# status = ccall((:InitializeUSB, "../software/libftdi_linux"), Culong, (Culong,), 0)
# status = ccall((:ConfigureUSB, "../software/libftdi_linux"), Culong, (Culong,), 0)
# IsOpen = Ref{Culong}(5);
# status = ccall((:GetIsOpen, "../software/libftdi_linux"), Culong, (Culong, Ref{Culong}), 0, IsOpen)

# # Send / read

# status = ccall((:PurgeRxTxUSB, "../software/libftdi_linux"), Culong, (Culong,), 0)

# BytesWritten = Ref{Culong}(5);
# BufferIn = collect(UInt8, linspace(1,10,10));
# BufferIn[1] = 5;
# BufferIn[end] = 255;
# status = ccall((:WriteUSB, "../software/libftdi_linux"), Culong,
#                (Culong, Ptr{Void}, Culong, Ref{Culong}),
#                0, BufferIn, length(BufferIn), BytesWritten)

# BytesRead = Ref{Culong}(5);
# BufferOut = zeros(UInt8, 10)
# status = ccall((:ReadUSB, "../software/libftdi_linux"), Culong,
#                (Culong, Ptr{Void}, Culong, Ref{Culong}),
#                0, BufferOut, 10, BytesRead)
